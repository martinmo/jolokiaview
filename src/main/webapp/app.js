// needed for flot to work with new jQuery
// http://stackoverflow.com/questions/14923301/uncaught-typeerror-cannot-read-property-msie-of-undefined-jquery-tools
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();


var app = angular.module('jolokiaViewApp', []);

app.controller('TreeCtrl', function ($scope, $http) {

    $scope.j4p = new Jolokia({url: "./jolokia"});

    $scope.domains = [];
    $scope.domainNames = [];
    $scope.domainFilter;

    $scope.selectedMbean;

    $scope.error;

    $scope.operations = [];

    $scope.operationResult;

    $scope.metrics = [];

    $scope.init = function () {
        var all = $scope.j4p.list();
        for (var domainName in all) {
            $scope.domainNames.push(domainName);
            var mBeanObjects = all[domainName];
            var domain = {
                name: domainName,
                mbeans: []
            };
            for (var mBeanName in mBeanObjects) {
                var mBean = mBeanObjects[mBeanName];

                var attrs = [];
                for (var attrName in mBean.attr) {
                    var attr = mBean.attr[attrName];
//                    console.log(attrName);
//                    console.log(attr);

                    attrs.push({
                        name: attrName,
                        value: "",
                        writeable: attr.rw,
                        type: attr.type
                    });
                }

                var ops = [];
                for (var operationName in mBean.op) {
                    var op = mBean.op[operationName];
                    ops.push({
                        name: operationName,
                        args: op.args
                    });
                }

                domain.mbeans.push({
                    name: mBeanName,
                    domain: domainName,
                    attrs: attrs,
                    ops: ops
                });
            }
            $scope.domains.push(domain);
        }
    };
    $scope.init();

    $scope.selectMBean = function (m) {
        $scope.error = {};
        $scope.selectedMbean = m;
        $scope.loadAttributes(m);
        $scope.operations = angular.copy(m.ops);
        $scope.operationResult = null;
    };

    $scope.loadAttributes = function (mbean) {
        var reqs = [];
        for (var i in mbean.attrs) {
            var attr = mbean.attrs[i];
            reqs.push({
                type: "read",
                mbean: mbean.domain + ":" + mbean.name, attribute: attr.name
            });
        }

        var response = $scope.j4p.request(reqs);
//        console.log(response);
        for (var i in mbean.attrs) {
            var attr = mbean.attrs[i];
            var value = response[i].value;
            if (response[i].status !== 200) {
                console.log("error");
                console.log(response[i]);
                attr.value = response[i].error;
            } else {
                attr.value = value;
            }
        }
    };

    $scope.executeOperation = function (mbean, operation) {
        $scope.operationResult = null;
        var args = [];
        for (var i in operation.args) {
            var arg = operation.args[i];
            var value = arg.value;
            if (typeof value === 'undefined') {
                value = "";
            }
            if (arg.type === "int" || arg.type === "long") {
                value = parseInt(value);
            }
            if (arg.type === "boolean") {
                value = (value === 'true');
            }
            args.push(value);
        }

        var req = {
            type: "exec",
            mbean: mbean.domain + ":" + mbean.name,
            operation: operation.name,
            arguments: args
        };
        console.log("req");
        console.log(req);
        var response = $scope.j4p.request(req);
        if (response.status === 200) {
            if (response.value === null) {
                $scope.operationResult = "NO RESULT";
            } else {
                $scope.operationResult = response.value;
            }
        } else {
            $scope.operationResult = {
                error: response.error,
                stacktrace: response.stacktrace
            };
        }
        $scope.modal($scope.operationResult);
    };

    $scope.modal = function (content) {
        $scope.modalcontent = content;
        $('#resultModal').modal('toggle');
    };

    $scope.print = function (e) {
        console.log(e);
    };

    $scope.data = [];
    $scope.dataRequests = [
        {
            type: "read",
            mbean: "java.lang:type=Memory",
            attribute: "HeapMemoryUsage",
            path: "used"
        }
        ,
        {
            type: "read",
            mbean: "java.lang:type=Memory",
            attribute: "HeapMemoryUsage",
            path: "max"
        }
        ,
        {
            type: "read",
            mbean: "java.lang:type=Memory",
            attribute: "HeapMemoryUsage",
            path: "committed"
        }
    ];

    $scope.run = function () {
        $scope.j4p.request($scope.dataRequests,
            {
                success: function (resp, i) {
                    console.log(resp);
                    console.log(i);
                    var value = resp.value / 1000;
                    var time = resp.timestamp * 1000;

                    if (!$scope.data[i]) {
                        var data = {
                            label: "" + resp.request.mbean + resp.request.attribute + "/" + resp.request.path,
                            data: [],
                            yaxis: (i ) % 2 + 1,
                            hoverable: true,
                            clickable: true
                        };
                        $scope.data[i] = data;
                    }
                    ;

                    var data = $scope.data[i];

                    data.data.push([time, value]);

                    if (i === ($scope.data.length - 1)) {
                        $.plot($("#charts"), $scope.data,
                            {
                                xaxis: { mode: "time" },
                                yaxes: [
                                    { },
                                    {
                                        position: "right",
                                        font: {
                                            size: 11,
                                            lineHeight: 13,
                                            style: "italic",
                                            weight: "bold",
                                            family: "sans-serif",
                                            variant: "small-caps",
                                            color: "red"
                                        }
                                    }
                                ],
                                grid: { backgroundColor: { colors: ["#fff", "#eee"] }},
                                series: {
                                    lines: { show: true },
                                    points: { show: false }
                                }
                            });
                        setTimeout($scope.run, 1000);
                    }
                }
            });

    };

    $scope.showPlots = function () {
        $scope.run();
        $('#plotModal').modal('toggle');
    }

});